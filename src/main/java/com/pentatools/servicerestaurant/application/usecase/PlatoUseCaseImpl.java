package com.pentatools.servicerestaurant.application.usecase;

import com.pentatools.servicerestaurant.domain.models.Plato;
import com.pentatools.servicerestaurant.domain.ports.in.PlatoUseCase;
import com.pentatools.servicerestaurant.domain.ports.out.PlatoRepositoryPort;

import java.util.List;
import java.util.Optional;

public class PlatoUseCaseImpl implements PlatoUseCase {

    private final PlatoRepositoryPort platoRepositoryPort;

    public PlatoUseCaseImpl(PlatoRepositoryPort platoRepositoryPort) {
        this.platoRepositoryPort = platoRepositoryPort;
    }

    @Override
    public Plato createPlato(Plato plato) {
        return platoRepositoryPort.save(plato);
    }

    @Override
    public Optional<Plato> getPlato(Long id) {
        return platoRepositoryPort.findById(id);
    }

    @Override
    public List<Plato> getAllPlato() {
        return platoRepositoryPort.findAll();
    }

    @Override
    public Optional<Plato> updatePlato(Long id, Plato plato) {
        return platoRepositoryPort.update(plato);
    }

    @Override
    public boolean deletePlato(Long id) {
        return platoRepositoryPort.deleteById(id);
    }
}
