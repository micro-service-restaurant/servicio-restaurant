package com.pentatools.servicerestaurant.application.service;

import com.pentatools.servicerestaurant.domain.models.Plato;
import com.pentatools.servicerestaurant.domain.ports.in.PlatoUseCase;

import java.util.List;
import java.util.Optional;

public class PlatoService implements PlatoUseCase {
    private final PlatoUseCase platoUseCase;

    public PlatoService(PlatoUseCase platoUseCase) {
        this.platoUseCase = platoUseCase;
    }


    @Override
    public Plato createPlato(Plato plato) {
        return platoUseCase.createPlato(plato);
    }

    @Override
    public Optional<Plato> getPlato(Long id) {
        return platoUseCase.getPlato(id);
    }

    @Override
    public List<Plato> getAllPlato() {
        return platoUseCase.getAllPlato();
    }

    @Override
    public Optional<Plato> updatePlato(Long id, Plato plato) {
        return platoUseCase.updatePlato(id, plato);
    }

    @Override
    public boolean deletePlato(Long id) {
        return platoUseCase.deletePlato(id);
    }
}
