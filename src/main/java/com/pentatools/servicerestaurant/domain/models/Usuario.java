package com.pentatools.servicerestaurant.domain.models;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Usuario {
    private Long id;

    @Column(length = 20)
    private String userName;

    private String email;

    private String information;

    /*public Usuario(Long id, String userName, String email, String information) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.information = information;
    }*/
}
