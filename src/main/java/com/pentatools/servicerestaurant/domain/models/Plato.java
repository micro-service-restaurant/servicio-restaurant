package com.pentatools.servicerestaurant.domain.models;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Plato {

    private Long id;
    private String nombre;
    private String descripcion;
    private double precio;
    private  String tipoPlato;
    private Long idUsuario;

    @JsonProperty
    private  Usuario usuario;


    /*public Plato(Long id, String nombre, String descripcion, double precio, String tipoPlato, Long idUsuario, Usuario usuario) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.tipoPlato = tipoPlato;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
    }*/
    // hhhhh jjjjj
}
