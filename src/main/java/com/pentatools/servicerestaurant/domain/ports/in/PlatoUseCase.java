package com.pentatools.servicerestaurant.domain.ports.in;

import com.pentatools.servicerestaurant.domain.models.Plato;

import java.util.List;
import java.util.Optional;

public interface PlatoUseCase {
    Plato createPlato(Plato plato);

    Optional<Plato> getPlato(Long id);

    List<Plato> getAllPlato();

    Optional<Plato> updatePlato(Long id, Plato plato);

    boolean deletePlato(Long id);
}
