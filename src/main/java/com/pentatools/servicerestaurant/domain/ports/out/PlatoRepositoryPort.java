package com.pentatools.servicerestaurant.domain.ports.out;

import com.pentatools.servicerestaurant.domain.models.Plato;

import java.util.List;
import java.util.Optional;

public interface PlatoRepositoryPort {

    Plato save(Plato plato);
    Optional<Plato> findById(Long id);

    List<Plato> findAll();
    Optional<Plato> update(Plato plato);
    boolean deleteById(Long id);
}
