package com.pentatools.servicerestaurant.infraestructure.entities;

import com.pentatools.servicerestaurant.domain.models.Plato;
import com.pentatools.servicerestaurant.domain.models.Usuario;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "platos")
public class PlatoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String descripcion;
    private double precio;
    private  String tipoPlato;

    private Long idUsuario;

    @Transient
    private Usuario usuario;

    public PlatoEntity(Long id, String nombre, String descripcion, double precio, String tipoPlato, Long idUsuario, Usuario usuario) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.tipoPlato = tipoPlato;
        this.idUsuario= idUsuario;
        this.usuario = usuario;
    }


    public static PlatoEntity fromDomainModel(Plato plato){
        return new PlatoEntity(plato.getId(), plato.getNombre(), plato.getDescripcion(), plato.getPrecio(), plato.getTipoPlato(), plato.getIdUsuario(), plato.getUsuario());
    }

    public Plato toDomainModel(){
        return new Plato(id, nombre, descripcion, precio, tipoPlato, idUsuario, usuario);
    }

}
