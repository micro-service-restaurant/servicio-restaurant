package com.pentatools.servicerestaurant.infraestructure.config;

import com.pentatools.servicerestaurant.application.service.PlatoService;
import com.pentatools.servicerestaurant.application.usecase.PlatoUseCaseImpl;
import com.pentatools.servicerestaurant.domain.ports.out.PlatoRepositoryPort;
import com.pentatools.servicerestaurant.infraestructure.repositories.PlatoRepositoryAdapter;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {
    @Bean
    public PlatoService platoService(PlatoRepositoryPort platoRepositoryPort){
        return new PlatoService(
                new PlatoUseCaseImpl(platoRepositoryPort)
                //new RetrieveTaskUseCaseImpl(taskRepositoryPort),
                //new UpdateTaskUseCaseImpl(taskRepositoryPort),
                //new DeleteTaskUseCaseImpl(taskRepositoryPort),
                //getAdditionalTaskInfoUseCase
        );
    }

    @Bean
    @Primary
    public PlatoRepositoryPort PlatoRepositoryPort(PlatoRepositoryAdapter platoRepositoryAdapter) {
        return platoRepositoryAdapter;
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
