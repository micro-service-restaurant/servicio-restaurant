package com.pentatools.servicerestaurant.infraestructure.controllers;

import com.pentatools.servicerestaurant.application.service.PlatoService;
import com.pentatools.servicerestaurant.domain.models.Plato;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/service-restaurant")
public class PlatoController {
    private final PlatoService platoService;

    public PlatoController(PlatoService platoService) {
        this.platoService = platoService;
    }

    @PostMapping
    public ResponseEntity<Plato> createPlato(@RequestBody Plato plato){
        Plato createdPlato = platoService.createPlato(plato);
        return new ResponseEntity<>(createdPlato, HttpStatus.CREATED);
    }

    @GetMapping("/{platoId}")
    public ResponseEntity<Plato> getPlatoId(@PathVariable Long platoId){
        return platoService.getPlato(platoId)
                .map(plato-> new ResponseEntity<>(plato, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public ResponseEntity<List<Plato>> getAllPlatos(){
        List<Plato> listPlato = platoService.getAllPlato();
        return new ResponseEntity<>(listPlato,HttpStatus.OK);
    }

    @PutMapping("/{platoId}")
    public ResponseEntity<Plato> updatePlato(@PathVariable Long platoId, @RequestBody Plato plato){
        return platoService.updatePlato(platoId, plato)
                .map(platoResult -> new ResponseEntity<>(platoResult, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{platoId}")
    public ResponseEntity<Void> deletePlatoById(@PathVariable Long platoId){
        if (platoService.deletePlato(platoId)){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
