package com.pentatools.servicerestaurant.infraestructure.repositories;

import com.pentatools.servicerestaurant.infraestructure.entities.PlatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatoRepository extends JpaRepository<PlatoEntity, Long> {
}
