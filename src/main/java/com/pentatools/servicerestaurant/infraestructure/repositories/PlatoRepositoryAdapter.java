package com.pentatools.servicerestaurant.infraestructure.repositories;

import com.pentatools.servicerestaurant.domain.models.Plato;
import com.pentatools.servicerestaurant.domain.models.Usuario;
import com.pentatools.servicerestaurant.domain.ports.out.PlatoRepositoryPort;
import com.pentatools.servicerestaurant.infraestructure.entities.PlatoEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PlatoRepositoryAdapter implements PlatoRepositoryPort {

    private Logger logger = LoggerFactory.getLogger(PlatoRepositoryAdapter.class);

    @Autowired
    private RestTemplate restTemplate;
    private final PlatoRepository platoRepository;

    public PlatoRepositoryAdapter(PlatoRepository platoRepository) {
        this.platoRepository = platoRepository;
    }


    @Override
    public Plato save(Plato plato) {
        PlatoEntity platoEntity = PlatoEntity.fromDomainModel(plato);
        PlatoEntity savedPlatoEntity = platoRepository.save(platoEntity);
        return savedPlatoEntity.toDomainModel();
    }

    @Override
    public Optional<Plato> findById(Long id) {
        return platoRepository.findById(id).map(PlatoEntity::toDomainModel);
    }

    @Override
    public List<Plato> findAll() {

        List<Plato> platos =
                platoRepository.findAll().stream()
                        .map(PlatoEntity::toDomainModel)
                        .collect(Collectors.toList());
        platos.stream().forEach((f) -> {
                    //ResponseEntity<Usuario> userRegistry1 = restTemplate.getForEntity("http://localhost:8083/desa/api/service-user/" + f.getIdUsuario(), Usuario.class);
                    ResponseEntity<Usuario> userRegistry1 = restTemplate.getForEntity("http://MICROSERVICIO-USUARIO/desa/api/service-user/" + f.getIdUsuario(), Usuario.class);
                    f.setUsuario(userRegistry1.getBody());
                }
        );
        return platos;
    }

    @Override
    public Optional<Plato> update(Plato plato) {
        if (platoRepository.existsById(plato.getId())){
            PlatoEntity platoEntity = PlatoEntity.fromDomainModel(plato);
            PlatoEntity updatePlatoEntity = platoRepository.save(platoEntity);
            return Optional.of(updatePlatoEntity.toDomainModel());
        }
        return Optional.empty();
    }

    @Override
    public boolean deleteById(Long id) {
        if (platoRepository.existsById(id)){
            platoRepository.deleteById(id);
        }
        return false;
    }
}
